/* test16colors.c - A.A.Shabarshin (Apr-May 2015) */

#include <stdio.h>
#include <math.h>

#define RGB(r,g,b) (r|(g<<8)|(b<<16))

unsigned long actual[16] = {
#if 0
 RGB(  4,   4,  12),
 RGB(  1,  19, 159),
 RGB( 70,   2, 172),
 RGB(  1,  86, 255),
 RGB(102,  15,  15),
 RGB(122, 133, 137),
 RGB(249,  49, 131),
 RGB(219, 118, 254),
 RGB(  0,  99,  34),
 RGB(  1, 207, 133),
 RGB(127, 133, 125),
 RGB( 15, 188, 253),
 RGB(138, 185,  54),
 RGB(102, 253,  90),
 RGB(253, 182,  74),
 RGB(220, 217, 202)
#else
 RGB(0  , 0  , 0  ), /* #000000 H=0   S=0   V=0   0000 */
 RGB(0  , 81 , 169), /* #0051A9 H=211 S=100 V=66  0001 */
 RGB(123, 6  , 199), /* #7B06C7 H=276 S=97  V=78  0010 */
 RGB(110, 88 , 255), /* #6E58FF H=248 S=65  V=100 0011 */
 RGB(140, 45 , 0  ), /* #8C2D00 H=19  S=100 V=55  0100 */
 RGB(127, 127, 127), /* #7F7F7F H=0   S=0   V=50  0101 */
 RGB(255, 52 , 157), /* #FF349D H=329 S=80  V=100 0110 */
 RGB(250, 134, 255), /* #FA86FF H=298 S=47  V=100 0111 */
 RGB(4  , 120, 0  ), /* #047800 H=118 S=100 V=47  1000 */
 RGB(0  , 202, 97 ), /* #00CA61 H=149 S=100 V=79  1001 */
 RGB(127, 127, 127), /* #7F7F7F H=0   S=0   V=50  1010 */
 RGB(114, 209, 255), /* #72D1FF H=200 S=55  V=100 1011 */
 RGB(144, 166, 0  ), /* #90A600 H=68  S=100 V=65  1100 */
 RGB(131, 248, 55 ), /* #83F837 H=96  S=78  V=97  1101 */
 RGB(255, 173, 85 ), /* #FFAD55 H=31  S=67  V=100 1110 */
 RGB(255, 255, 255)  /* #FFFFFF H=0   S=0   V=100 1111 */
#endif
};

#if 1

/*
 EGA palette:
 0x00 -> 0
 0x55 -> 85
 0xAA -> 170
 0xFF -> 255
*/

unsigned long desirable[16] = {
 RGB(  0,  0,  0),
 RGB(  0,  0,170),
 RGB(  0,170,  0),
 RGB(  0,170,170),
 RGB(170,  0,  0),
 RGB(170,  0,170),
 RGB(170, 85,  0),
 RGB(170,170,170),
 RGB( 85, 85, 85),
 RGB( 85, 85,255),
 RGB( 85,255, 85),
 RGB( 85,255,255),
 RGB(255, 85, 85),
 RGB(255, 85,255),
 RGB(255,255, 85),
 RGB(255,255,255)
};

/*

E[0]	#00 ERR=0	ERR'=0  
E[1]	#21 ERR=77	ERR'=81 *
E[2]	#89 ERR=49	ERR'=50 *
E[3]	#91 ERR=47	ERR'=79 *
E[4]	#44 ERR=54	ERR'=54  
E[5]	#62 ERR=36	ERR'=55 *
E[6]	#C4 ERR=35	ERR'=85 *
E[7]	#EB ERR=26	ERR'=74 *
E[8]	#41 ERR=27	ERR'=72 *
E[9]	#33 ERR=25	ERR'=25  
E[10]	#D9 ERR=37	ERR'=55 *
E[11]	#BB ERR=54	ERR'=54  
E[12]	#6E ERR=45	ERR'=79 *
E[13]	#77 ERR=49	ERR'=49  
E[14]	#ED ERR=78	ERR'=82 *
E[15]	#FF ERR=0	ERR'=0  

*/

#else

/* Palette from games of Lucas O. Wagner */

unsigned long desirable[16] = {
/*  0 [BLACK]     */ RGB(0, 0, 0),
/*  1 [GRAY]      */ RGB(157, 157, 157),
/*  2 [WHITE]     */ RGB(255, 255, 255),
/*  3 [PINK]      */ RGB(224, 111, 139),
/*  4 [RED]       */ RGB(190, 38, 51),
/*  5 [BLAZE]     */ RGB(235, 137, 49),
/*  6 [BROWN]     */ RGB(164, 100, 34),
/*  7 [DULLBROWN] */ RGB(73, 60, 43),
/*  8 [GINGER]    */ RGB(247, 226, 107),
/*  9 [SLIME]     */ RGB(163, 206, 39),
/* 10 [GREEN]     */ RGB(68, 137, 26),
/* 11 [BLUEGREEN] */ RGB(47, 72, 78),
/* 12 [CLOUDBLUE] */ RGB(178, 220, 239),
/* 13 [SKYBLUE]   */ RGB(49, 162, 242),
/* 14 [SEABLUE]   */ RGB(0, 87, 132),
/* 15 [INDIGO]    */ RGB(38, 25, 50)
};

/*

E[0]	#00 ERR=0	ERR'=0  
E[1]	#3D ERR=38	ERR'=174 *
E[2]	#FF ERR=0	ERR'=382  
E[3]	#E6 ERR=36	ERR'=245 *
E[4]	#46 ERR=30	ERR'=71 *
E[5]	#4E ERR=47	ERR'=228 *
E[6]	#E4 ERR=36	ERR'=76 *
E[7]	#A0 ERR=23	ERR'=120 *
E[8]	#CF ERR=54	ERR'=156 *
E[9]	#DC ERR=28	ERR'=251 *
E[10]	#0D ERR=13	ERR'=130 *
E[11]	#A0 ERR=24	ERR'=233 *
E[12]	#BF ERR=21	ERR'=202 *
E[13]	#1B ERR=35	ERR'=203 *
E[14]	#11 ERR=37	ERR'=273  
E[15]	#05 ERR=48	ERR'=376 *

*/

#endif

int main()
{
 int i,j,mf,me,mj,r,g,b;
 double d,c1,c2;

 printf("\n1-color mix:\n\n\t");
 for(i=0;i<16;i++) printf("X[%i]\t",i);
 printf("\n");
 for(i=0;i<16;i++)
 {
   printf("E[%i]\t",i,actual[i]);
   me = 1000;
   mj = -1;
   for(j=0;j<16;j++)
   {
      d = 0;
      c1 = actual[j]&255;
      c2 = desirable[i]&255;
      d += (c1-c2)*(c1-c2);
      c1 = (actual[j]>>8)&255;
      c2 = (desirable[i]>>8)&255;
      d += (c1-c2)*(c1-c2);
      c1 = (actual[j]>>16)&255;
      c2 = (desirable[i]>>16)&255;
      d += (c1-c2)*(c1-c2);
      d = sqrt(d);
      printf("%3.2lf\t",d);
      if(d < me)
      {
        me = (int)d;
        if(d-me >= 0.5) me++;
        mj = j;
      }
   }
   printf("[%i]\n",mj);
 }

 printf("\n2-color mix:\n\n");
 for(i=0;i<16;i++)
 {
   me = 1000;
   mj = -1;
   for(j=0;j<256;j++)
   {
      d = 0;
      c1 = ((actual[j&15]&255)+(actual[j>>4]&255))/2.0;
      r = c1; if(c1-r>=0.5) r++; if(r>255) r=255;
      c2 = desirable[i]&255;
      d += (c1-c2)*(c1-c2);
      c1 = (((actual[j&15]>>8)&255)+((actual[j>>4]>>8)&255))/2.0;
      g = c1; if(c1-g>=0.5) g++; if(g>255) g=255;
      c2 = (desirable[i]>>8)&255;
      d += (c1-c2)*(c1-c2);
      c1 = (((actual[j&15]>>16)&255)+((actual[j>>4]>>16)&255))/2.0;
      b = c1; if(c1-b>=0.5) b++; if(b>255) b=255;
      c2 = (desirable[i]>>16)&255;
      d += (c1-c2)*(c1-c2);
      d = sqrt(d);
      switch(i)
      {
        case 0: if(j==0x00) mf=(int)d; break;
        case 1: if(j==0x11) mf=(int)d; break;
        case 2: if(j==0x88) mf=(int)d; break;
        case 3: if(j==0x99) mf=(int)d; break;
        case 4: if(j==0x44) mf=(int)d; break;
        case 5: if(j==0x22) mf=(int)d; break;
        case 6: if(j==0xCC) mf=(int)d; break;
        case 7: if(j==0x55) mf=(int)d; break;
        case 8: if(j==0xAA) mf=(int)d; break;
        case 9: if(j==0x33) mf=(int)d; break;
        case 10:if(j==0xDD) mf=(int)d; break;
        case 11:if(j==0xBB) mf=(int)d; break;
        case 12:if(j==0x66) mf=(int)d; break;
        case 13:if(j==0x77) mf=(int)d; break;
        case 14:if(j==0xEE) mf=(int)d; break;
        case 15:if(j==0xFF) mf=(int)d; break;
      }
      if(d < me)
      {
        me = (int)d;
        if(d-me >= 0.5) me++;
        mj = j;
      }
      if(i==0) printf("%3d %3d %3d [%i] 0x%2.2X\n",r,g,b,j,j);
      if(i==0 && j==255) printf("\n");
   }
   printf("E[%i]\t#%2.2X ERR=%i\tERR'=%i %c\n",i,mj,me,mf,((mj>>4)==(mj&15))?' ':'*');
 }

 return 0;
}

