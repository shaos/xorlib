/* colodiag.c - A.A.Shabarshin (October 2022) */

/* THIS PUBLIC DOMAIN SOURCE CODE IS PROVIDED AS IS */

#include "xorlib.h"

int main()
{

 int i,y;
 unsigned char *p;

 xoinit(XOMODE_160x200_COL15); /* gray colors 5 and 10 are identical */

/*
 0 - Default composite mode colors
 1 - Similar to CGA composite mode colors
 2 - Similar to Tandy composite mode colors
 3 - Similar to PCjr composite mode colors
*/
 xopalette(0);

 for(y=0;y<200;y++)
 {
   p = xodirectline(y);
   if(y<2||y>=198)
   {   /* white frame */
       for(i=0;i<80;i++) p[i] = 0xFF;
       continue;
   }
   p[0] = 0xF0;
   p[79] = 0x0F;

   if(y>=2 && y<14) for(i=1;i<79;i++) p[i]=0x00;
   if(y>=15 && y<26) for(i=1;i<79;i++) p[i]=0x11;
   if(y>=27 && y<38) for(i=1;i<79;i++) p[i]=0x22;
   if(y>=39 && y<50) for(i=1;i<79;i++) p[i]=0x33;
   if(y>=51 && y<62) for(i=1;i<79;i++) p[i]=0x44;
   if(y>=63 && y<74) for(i=1;i<79;i++) p[i]=0x55;
   if(y>=75 && y<86) for(i=1;i<79;i++) p[i]=0x66;
   if(y>=87 && y<98) for(i=1;i<79;i++) p[i]=0x77;
   if(y>=99 && y<110) for(i=1;i<79;i++) p[i]=0x88;
   if(y>=111 && y<122) for(i=1;i<79;i++) p[i]=0x99;
   if(y>=123 && y<134) for(i=1;i<79;i++) p[i]=0xAA;
   if(y>=135 && y<146) for(i=1;i<79;i++) p[i]=0xBB;
   if(y>=147 && y<158) for(i=1;i<79;i++) p[i]=0xCC;
   if(y>=159 && y<170) for(i=1;i<79;i++) p[i]=0xDD;
   if(y>=171 && y<182) for(i=1;i<79;i++) p[i]=0xEE;
   if(y>=183 && y<194) for(i=1;i<79;i++) p[i]=0xFF;

   for(i=y+5;i<y+35;i++) xopixel(i,y,i&1);   
   
   for(i=y+70;i<y+170;i++) xopixel(i,y,0);
   for(i=y+170;i<y+270;i++) xopixel(i,y,i&1);
   for(i=y+270;i<y+370;i++) xopixel(i,y,1);
   
   for(i=y+405;i<y+435;i++) xopixel(i,y,(i&1)?0:1);
 }

 return 0;
}
