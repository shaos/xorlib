/* demo2016.c - A.A.Shabarshin (October-November 2016) */

/* THIS PUBLIC DOMAIN SOURCE CODE IS PROVIDED AS IS */

#include <string.h>
#include <stdio.h>
#include "xorlib.h"
#include "nedofont.h"
#include "demo2016/shaos.c"
#include "demo2016/moon.c"

const char demo[] = "                                           "
"                                                                "
"SHAOS PRESENTS extremely low cost game console Xorya that uses "
"PIC32 microcontroller in DIP28 package with few other cheap components and "
"programmable in C with help of open source game library XORLib. "
"Xorya may be easily assembled on a small solderless breadboard and connected "
"to NTSC TV to display 320x200 or 640x200 monochrome screen, "
"but with enabled color burst latter mode may confuse NTSC TV to recognize "
"15 different colors with effective screen resolution 160x200 pixels. "
"This particular demo was specifically crafted for Hackaday Superconference 2016 "
"that is taking place in Pasadena, CA. "
"Special thanks to Hackaday, Microchip Technology Inc, "
"Lucio Di Jasio, Bruce Land, KenKen and Arnaud Durand! "
"For more info go to XORYA.COM "
"                          ";
int demo_draw = 0;
char demo_char;
#define DEMOBOARD 160
char board[DEMOBOARD];
char board_[DEMOBOARD];
unsigned char buf[10000];

/* Lets keep it in RAM */
unsigned char sinus[90] = { 0 , 4 , 8 , 13 , 17 , 22 , 26 , 31 , 35 , 40 , 44 , 48 , 53 ,
  57 , 61 , 66 , 70 , 74 , 79 , 83 , 87 , 91 , 95 , 100 , 104 , 108 , 112 ,
  116 , 120 , 124 , 128 , 131 , 135 , 139 , 143 , 146 , 150 , 154 , 157 ,
  161 , 164 , 167 , 171 , 174 , 177 , 181 , 184 , 187 , 190 , 193 , 196 ,
  198 , 201 , 204 , 207 , 209 , 212 , 214 , 217 , 219 , 221 , 223 , 226 ,
  228 , 230 , 232 , 233 , 235 , 237 , 238 , 240 , 242 , 243 , 244 , 246 ,
  247 , 248 , 249 , 250 , 251 , 252 , 252 , 253 , 254 , 254 , 255 , 255 ,
  255 , 255 , 255 };

/* Macros for faster calculations */
#define SIN(x) ((x<90)?sinus[x]:((x<180)?sinus[179-x]:((x<270)?(-sinus[x-180]):(-sinus[359-x]))))
#define COS(x) ((x<90)?sinus[89-x]:((x<180)?(-sinus[x-90]):((x<270)?(-sinus[269-x]):sinus[x-270])))

int div10[160]; /* to make fast division by 10 */

void coloroto(int frames)
{
 register unsigned char* p;
 register int x,x2,c,r;
 register short co,si;
 int y,s=30,a=0;
 unsigned long f=0;
 /* fill array for faster division by 10 */
 for(c=0;c<160;c++) div10[c] = c/10;
/*
 X' = X*COS(a) - Y*SIN(a) + X0
 Y' = X*SIN(a) + Y*COS(a) + Y0
*/
 while(frames--)
 {
  co = COS(a); /* precalculate cosine here */
  si = SIN(a); /* precalculate sine here */
  for(y=0;y<200;y++) /* go through every horizontal line */
  {
   p = xodirectline(y); /* get pointer to the videomemory of line Y */
   if(!p) break;
   x2 = 80 - (((y-100)*si)>>9); /* this is Y portion of the formula */
   for(x=0;x<160;x+=2) /* go through every 2 pixels in this line */
   {
     /* we need only X' to get color value for bar, so we will ignore Y' for now */
     c = (((x-80)*co)>>8) + x2; /* x2 was calculated before this loop */
     if(c<0 || c>=160) r = 0x50; /* lets be gray outside of the color chart */
     else r = div10[c]<<4; /* calculate color value of 1st pixel (0...15) */
     /* calculate color of 2nd pixel and store assembled byte with 2 pixels */
     c = (((x-79)*co)>>8) + x2; /* x2 was calculated before this loop */
     if(c<0 || c>=160) p[x>>1] = r|5; /* lets be gray outside of the color chart */
     else p[x>>1] = r|div10[c]; /* calculate color value of 2nd pixel (0...15) */
   }
  }
  if(++a==360) a = 0; /* rotate and check that it's in the range */
  /* wait some number of frames in the beginning */
  if(s>1)
  {
    f = xoframes() + (255-sinus[89-s--]);
    while(xoframes() < f);
  }
  xowaitretrace();
 }
}

#define FIXED32
//#define FIXED64
#define FAKEHD

/* 27 colors (14 solid and 13 dithered) for Mandelbrot Set visualization */

#ifdef FIXED32
/* just to make it a little faster for 32-bit version (216 iterations) */
#define MANLIMIT (27<<3)
#define MANCOLOR1(x) colors1[(x)>>3]
#define MANCOLOR2(x) colors2[(x)>>3]
#else
/* regularly it is 432 iterations max */
#define MANLIMIT (27<<4)
#define MANCOLOR1(x) colors1[(x)>>4]
#define MANCOLOR2(x) colors2[(x)>>4]
#endif

int colors[] = {15,15,6,6,7,7,2,2,3,3,1,1,11,11,9,9,8,8,13,13,12,12,14,14,4,4,0,0};

/* Fixed Point 4.28 */
#ifdef FIXED32
#define FIXED
#define REAL long
#define TOREAL(x) (long)((x)*268435456.0)
#define MUREAL(x,y) ((x)>>14)*((y)>>14)
#endif

/* Fixed Point 8.56 */
#ifdef FIXED64
#define FIXED
#define REAL long long
#if 0 /* this is not working in WATCOM-C v19 for negative numbers, so use longer version */
#define TOREAL(x) (long long)(((long double)(x))*72057594037927936.0)
#else
#define TOREAL(x) (((x)<0)?(-(long long)((0.0-(x))*72057594037927936.0)):((long long)((x)*72057594037927936.0)))
#endif
#define MUREAL(x,y) ((x)>>28)*((y)>>28)
#endif

/* 64-bit Floating Point */
#ifndef FIXED
#define REAL long double
#define TOREAL(x) ((long double)(x))
#define MUREAL(x,y) (x)*(y)
#endif

void mandelbrot(int seconds)
{
 char s[16];
 int y,c1,c2,c1n,c2n,dt;
 unsigned char *p,saved[80];
 unsigned long t0,t1;
 register int i,j;
 REAL x2,y2,d0,d1,d2,four; /* better be fast */
 REAL x0,y0,dx,dy,sx,sy,x1,y1,xc,yc,m,ratio,hstep,vstep;
 int *colors1 = &colors[1];
 int *colors2 = &colors[0];
 t0 = xoseconds();    
#if 1
 x0 = TOREAL(-3); y0 = TOREAL(1.5); dx = TOREAL(4); /* start view */
#else
// x0 = TOREAL(-0.219759); y0 = TOREAL(-0.813599); dx = TOREAL(0.000977);
// x0 = TOREAL(0.30026); y0 = TOREAL(0.025391); dx = TOREAL(0.007812);
 x0 = TOREAL(-0.175); y0 = TOREAL(-1.023438); dx = TOREAL(0.03125);
#endif
 xc = TOREAL(-0.163); /* x coord of point of interest */
 yc = TOREAL(-1.035); /* y coord of point of interest */
 m = TOREAL(0.97); /* scale coefficient between frames */
 four = TOREAL(4); /* value to check that point is outside */
#ifdef FAKEHD
 ratio = TOREAL(9.0/16); /* assume 16:9 ratio (for widescreen TV) */
#else
 ratio = TOREAL(3.0/4); /* assume 4:3 ratio (for standard TV) */
#endif
 hstep = TOREAL(1.0/158); /* horizontal step for 158 pixels */
 vstep = TOREAL(1.0/98); /* vertical step for 98 pixels */
 while(1)
 {
  dy = MUREAL(dx,ratio);
  sx = MUREAL(dx,hstep);
  sy = MUREAL(dy,vstep);
  y1 = y0;
  for(y=0;y<200;y++)
  {
   p = xodirectline(y);
   if(y<2||y>=198)
   {   /* white frame */
       for(i=0;i<80;i++) p[i] = 0xFF;
       continue;
   }
   x1 = x0;
   if(y&1) for(i=0;i<80;i++) p[i] = saved[i];
   else 
   {
     for(i=0;i<80;i++)
     {
/*     Every iteration here do 2 pixels and for each:
  
       Z[j+1] = Z[j]*Z[j] + C
       C is (x1,y1)
       Z is (x2,y2)
       Z[j]*Z[j] => (x2+y2*i)*(x2+y2*i) => x2*x2 + 2*x2*y2*i - y2*y2 => (x2*x2 - y2*y2, 2*x2*y2)
       Z[j]*Z[j] + C => (x2*x2 - y2*y2 + x1, 2*x2*y2 + y1)
*/
       if(i==0) c1=c1n=15;
       else
       {
         x2 = y2 = 0;
         j = 0;
         while(j++ < MANLIMIT)
         {
           d1 = MUREAL(x2,x2);
           d2 = MUREAL(y2,y2);
           if(d1 + d2 > four) break;
           d0 = MUREAL(x2,y2);
           x2 = d1 - d2 + x1;
           y2 = d0 + d0 + y1;
         }
         if(j>=MANLIMIT) c1=c1n=0;
         else
         {
           c1 = MANCOLOR1(j);
           c1n = MANCOLOR2(j);
         }
         x1 += sx;
       }
       if(i==79) c2=c2n=15;
       else
       {
         x2 = y2 = 0;
         j = 0;
         while(j++ < MANLIMIT)
         {
           d1 = MUREAL(x2,x2);
           d2 = MUREAL(y2,y2);
           if(d1 + d2 > four) break;
           d0 = MUREAL(x2,y2);
           x2 = d1 - d2 + x1;
           y2 = d0 + d0 + y1;
         }
         if(j>=MANLIMIT) c2=c2n=0;
         else
         {
           c2 = MANCOLOR1(j);
           c2n = MANCOLOR2(j);
         }
         x1 += sx;
       }
       p[i] = (c1<<4)|c2;
       saved[i] = (c1n<<4)|c2n;
     }
     y1 -= sy;
   }
  }
#if 1
  *((int*)s) = 2037542744; /* little-endian magic number ;) */
  t1 = xoseconds();
  dt = (int)(t1 - t0); /* show minutes:seconds.hundreds (approx) */
  if(dt>=seconds) return;
  sprintf(s+4,"%c %i:%02d.%02d",97,dt/60,dt%60,(xoframes()%60)*5/3);
  xostring(1,23,s);
#endif
  x0 = xc - MUREAL(xc-x0,m);
  y0 = yc - MUREAL(yc-y0,m);
  dx = MUREAL(dx,m);
 } 
}

#define MAXM 50

int main()
{
register int i,j,k,l,o,x,y,c;
register unsigned char *p,*pp;
int xm[MAXM],ym[MAXM],fm,nm,im,jm,km,rm,mm;
unsigned int tm;
int w,m,state = 0;
unsigned int t, f = 0;
unsigned long t0,t1,t2;
const char *pshaos;
const char *tshaos;
char str[32];

xoinit(XOMODE_160x200_COL15);

AGAIN:

f = 0;
tm = 0;
fm = nm = 0;
state = 0;
demo_draw = 0;
for(i=0;i<DEMOBOARD;i++) board[i]=board_[i]=0;
while(1)
{
 pshaos = shaos;
 switch(state)
 {
     case 0: tshaos = &shaos[89*144]; break;   
     case 1: tshaos = shaos_l; break;   
     case 2: tshaos = &shaos[89*144]; break;   
     case 3: tshaos = shaos_r; break;   
 }
 if((f++)&1){pshaos++;tshaos++;}
 for(y=0;y<200;y++)
 {
   p = xodirectline(y);
#if 0
   if(y<2||y>=198)
   {   /* white frame */
       for(i=0;i<80;i++) p[i] = 0xFF;
       continue;
   }
   p[0] = 0xF0;
   for(i=1;i<5;i++) p[i] = 0x00;
   for(i=5;i<10;i++) p[i] = 0x44;//0x11;
   for(i=10;i<15;i++) p[i] = 0xEE;//0x22;
   for(i=15;i<20;i++) p[i] = 0xFF;//0x33;
   for(i=20;i<25;i++) p[i] = 0xEE;//0x44;
   for(i=25;i<30;i++) p[i] = 0x44;//0x55;
   for(i=30;i<35;i++) p[i] = f?0x04:0x40;//0x66;
   for(i=35;i<40;i++) p[i] = f?0x4E:0xE4;//0x77;
   for(i=40;i<45;i++) p[i] = f?0xEF:0xFE;//0x88;
   for(i=45;i<50;i++) p[i] = f?0xAA:0x55;//0x99;
   for(i=50;i<55;i++) p[i] = 0xAA;
   for(i=55;i<60;i++) p[i] = f?0x00:0xFF;//0xBB;
   for(i=60;i<65;i++) p[i] = 0xCC;
   for(i=65;i<70;i++) p[i] = 0xDD;
   for(i=70;i<75;i++) p[i] = 0xEE;
   for(i=75;i<80;i++) p[i] = 0xFF;
#else
   if(y<171)
   {
      for(i=22;i<58;i++)
      {
       if(f>320) switch(*pshaos)
       {
           case '.': c=0x00; break;
           case '+': c=0x40; break;
           case '@': c=0xE0; break;
           case '#': c=0xF0; break;
           case '?': c=0xA0; break;
       }
       else if(f>280) switch(*pshaos)
       {
           case '.': c=0x00; break;
           case '+': c=0x00; break;
           case '@': c=0x40; break;
           case '#': c=0xE0; break;               
       }
       else if(f>240 && (*pshaos)=='#') c=0x40;
       else c=0x00;
       pshaos++;
       pshaos++;
       if(f>340) switch(*pshaos)
       {
           case '.': break;
           case '+': c|=0x04; break;
           case '@': c|=0x0E; break;
           case '#': c|=0x0F; break;
           case '?': c|=0x0A; break;
       }
       else if(f>300) switch(*pshaos)
       {
           case '.': break;
           case '+': break;
           case '@': c|=0x04; break;
           case '#': c|=0x0E; break;               
       }
       else if(f>260 && (*pshaos)=='#') c|=0x04;
       pshaos++;
       pshaos++;    
       if(y>=89 && y<96 && (state==1 || state==3))
       {
           switch(*tshaos)
           {
           case '.': c=0x00; break;
           case '+': c=0x40; break;
           case '@': c=0xE0; break;
           case '#': c=0xF0; break;
           case '?': c=0xA0; break;
           }
           tshaos++;
           tshaos++;
           switch(*tshaos)
           {
           case '.': break;
           case '+': c|=0x04; break;
           case '@': c|=0x0E; break;
           case '#': c|=0x0F; break;
           case '?': c|=0x0A; break;
           }
           tshaos++;
           tshaos++;
       }
       p[i] = c;
      }
   }
#endif
 }
/*
  7 sec - normal (0)
  1 sec - left   (1)
  5 sec - normal (2)
  4 sec - right  (3)
  ------------------
  1024 frames total
*/
 if(f>360)
 {
  t = (f-360)&1023;
  switch(state)
  {
   case 0:
      if(t > 420) state = 1;
      break;

   case 1:
      if(t > 480) state = 2;
      break;

   case 2:
      if(t > 780) state = 3;
      break;

   case 3:
      if(t < 420) state = 0;
      break;
  }
 }
 
 demo_char = demo[demo_draw>>3];
 if(demo_char) demo_draw++;
#if 0
 else demo_draw = 0;
#else
 else break; // go to next demo
#endif
 if(demo_char!=' ') fm++;
 for(i=0;i<DEMOBOARD;i++) board_[i]=board_[i+1];
 k = 0x80 >> (demo_draw & 7);
 if(demo_char>=32)
      i = demo_char - 32;
 else i = 0;
 board_[DEMOBOARD-1] = 
         (((font8x8[i][1]&k)?1:0)<<6)|
         (((font8x8[i][2]&k)?1:0)<<5)|
         (((font8x8[i][3]&k)?1:0)<<4)|
         (((font8x8[i][4]&k)?1:0)<<3)|
         (((font8x8[i][5]&k)?1:0)<<2)|
         (((font8x8[i][6]&k)?1:0)<<1)|
          ((font8x8[i][7]&k)?1:0);

#define XSTEP 4
#define YSTEP 3
#define BIGDOT(x,y,c) {int xx = x>>3;\
 p = xodirectline(y);\
 if(x&4) p[xx]=(p[xx]&0xF0)|(c?0x02:0x00);\
 else  p[xx]=(c?0x20:0x00)|(p[xx]&0x0F);\
 p = xonextline(p);\
 if(x&4) p[xx]=(p[xx]&0xF0)|(c?0x02:0x00);\
 else  p[xx]=(c?0x20:0x00)|(p[xx]&0x0F);\
 p = xonextline(p);\
 if(x&4) p[xx]=(p[xx]&0xF0)|(c?0x02:0x00);\
 else  p[xx]=(c?0x20:0x00)|(p[xx]&0x0F);\
 }
 
 j = 0;
 for(k=0;k<DEMOBOARD;k++)
 {
   x = k*XSTEP;
   if(board[k]!=board_[k])
   {
       y = 179;
       if((board[k]&0x40)!=(board_[k]&0x40))
       {
           if(board_[k]&0x40) c=1;
           else c=0;
           BIGDOT(x,y,c);
           j++;
       }
       y += YSTEP;
       if((board[k]&0x20)!=(board_[k]&0x20))
       {
           if(board_[k]&0x20) c=1;
           else c=0;
           BIGDOT(x,y,c);
           j++;
       }
       y += YSTEP;
       if((board[k]&0x10)!=(board_[k]&0x10))
       {
           if(board_[k]&0x10) c=1;
           else c=0;
           BIGDOT(x,y,c);
           j++;
       }
       y += YSTEP;
       if((board[k]&0x08)!=(board_[k]&0x08))
       {
           if(board_[k]&0x08) c=1;
           else c=0;
           BIGDOT(x,y,c);
           j++;
       }
       y += YSTEP;
       if((board[k]&0x04)!=(board_[k]&0x04))
       {
           if(board_[k]&0x04) c=1;
           else c=0;
           BIGDOT(x,y,c);
           j++;
       }
       y += YSTEP;
       if((board[k]&0x02)!=(board_[k]&0x02))
       {
           if(board_[k]&0x02) c=1;
           else c=0;
           BIGDOT(x,y,c);
           j++;
       }
       y += YSTEP;
       if((board[k]&0x01)!=(board_[k]&0x01))
       {
           if(board_[k]&0x01) c=1;
           else c=0;
           BIGDOT(x,y,c);
           j++;
       }
       board[k] = board_[k];
   }
 }
 
 if(fm)
 {
  if(f>=tm)
  {
     tm = f + 1 + (rand()%20);
     im = rand()%42;
     if(im>=21) im=100-im;
     for(jm=0;jm<nm;jm++)
         if(xm[jm]<0) break;
     xm[jm] = im;
     ym[jm] = 0;
     if(nm==jm) nm++;
  }
  if((f&7)==0)
  {
      for(jm=0;jm<nm;jm++)
      {
         km = ym[jm];
         if(km<22)
         {
           im = rand()&255;
           if(im<=32) im=255-im;
           sprintf(str,"%c",im);
           xostring(xm[jm],km,str);
         }
         if(km>0 && km<23)
         {
             im = xm[jm]<<3;
             rm = km<<3;
             im+=2;
             xoline(im,rm-1,im,rm-8,0);
             im+=4;
             xoline(im,rm-1,im,rm-8,0);
         }
         km -= 6;
         if(km>0 && km<23)
         {
             mm = -1;
             for(rm=0;rm<nm;rm++)
             {
                 if(xm[rm]>=0 && rm!=jm && xm[rm]==xm[jm] && ym[rm]<ym[jm] && ym[rm]>mm) mm=ym[rm];
             }
             if(mm < km-1)
             {
              im = xm[jm]<<3;
              rm = km<<3;
              im+=1;
              xoline(im,rm-1,im,rm-8,0);
              im+=4;
              xoline(im,rm-1,im,rm-8,0);
             }
         }
         km -= 6;
         if(km>0 && km<23)
         {
             mm = -1;
             for(rm=0;rm<nm;rm++)
             {
                 if(xm[rm]>=0 && rm!=jm && xm[rm]==xm[jm] && ym[rm]<ym[jm] && ym[rm]>mm) mm=ym[rm];
             }
             if(mm < km-1)
             {
              im = xm[jm]<<3;
              rm = km<<3;
              im+=3;
              xoline(im,rm-1,im,rm-8,0);
              im+=4;
              xoline(im,rm-1,im,rm-8,0);
             }
         }
         km -= 6;
         if(km>0 && km<23)
         {
             mm = -1;
             for(rm=0;rm<nm;rm++)
             {
                 if(xm[rm]>=0 && rm!=jm && xm[rm]==xm[jm] && ym[rm]<ym[jm] && ym[rm]>mm) mm=ym[rm];
             }
             if(mm < km-1)
             {
              im = xm[jm]<<3;
              rm = km<<3;
              xoline(im,rm-1,im,rm-8,0);
              im+=4;
              xoline(im,rm-1,im,rm-8,0);
             }
         }
         if(++ym[jm]>40) xm[jm]=-1;
      }
  }
}
 
#if 0
 sprintf(str,"%i %i %i %i  ",f,xoframes(),xocurline(),nm);
 xostring(0,0,str);
#endif
 xowaitretrace();
}

for(k=0;k<3200;k++)
{
 for(y=0;y<200;y++)
 {
   p = xodirectline(y);
   if(k<200 && y==(k%200)) for(i=0;i<5;i++) p[i] = 0x00;
   else if(k<400 && y==(k%200)) for(i=5;i<10;i++) p[i] = 0x11;
   else if(k<600 && y==(k%200)) for(i=10;i<15;i++) p[i] = 0x22;
   else if(k<800 && y==(k%200)) for(i=15;i<20;i++) p[i] = 0x33;
   else if(k<1000 && y==(k%200)) for(i=20;i<25;i++) p[i] = 0x44;
   else if(k<1200 && y==(k%200)) for(i=25;i<30;i++) p[i] = 0x55;
   else if(k<1400 && y==(k%200)) for(i=30;i<35;i++) p[i] = 0x66;
   else if(k<1600 && y==(k%200)) for(i=35;i<40;i++) p[i] = 0x77;
   else if(k<1800 && y==(k%200)) for(i=40;i<45;i++) p[i] = 0x88;
   else if(k<2000 && y==(k%200)) for(i=45;i<50;i++) p[i] = 0x99;
   else if(k<2200 && y==(k%200)) for(i=50;i<55;i++) p[i] = 0xAA;
   else if(k<2400 && y==(k%200)) for(i=55;i<60;i++) p[i] = 0xBB;
   else if(k<2600 && y==(k%200)) for(i=60;i<65;i++) p[i] = 0xCC;
   else if(k<2800 && y==(k%200)) for(i=65;i<70;i++) p[i] = 0xDD;
   else if(k<3000 && y==(k%200)) for(i=70;i<75;i++) p[i] = 0xEE;
   else if(y==(k%200)) for(i=75;i<80;i++) p[i] = 0xFF;
 }
 if(k>=200 && !(k&15)) xowaitretrace();
}

t1 = xoseconds() + 7;
while(xoseconds() < t1);
coloroto(1800);

mandelbrot(60);

t0 = xoframes();
while(1)
{
    int dt = ((int)(xoframes()-t0))-600;
    if(dt>500) dt-=(dt-500)/2;
    y = rand()%200;
    if((y>=200-dt && y<250-dt)||
       (y>=450-dt && y<500-dt)||
       (y>=700-dt && y<750-dt)
      )
    {
        if(dt==625) break;
        memset(xodirectline(y),0,80);
        continue;
    }
    xodirectline(y)[rand()%80]=rand()%255;
}
t0 = xoframes();
while(1)
{
    int dt = (int)(xoframes()-t0)/2;
    if(dt>75) break;
    y = rand()%200;
    if(y>=75-dt && y<125+dt)
    {
        memset(xodirectline(y),0,80);
        if(y==99) xodirectline(y)[39]=0x0E;
        continue;
    }
    xodirectline(y)[rand()%80]=rand()%255;
}
p = xodirectline(98);
p[39] = 0x0E;
p = xonextline(p);
p[39] = 0xEE;
p[40] = 0xE0;
p = xonextline(p);
p[39] = 0x0E;
t2 = xoseconds() + 3;
while(xoseconds() < t2);
memset(buf,0,10000);
int steps[4]={3292,918,256,71};
int stepw[4]={8,28,100,360};
int stepx1[4]={38,33,15,-50};
int stepx2[4]={42,47,65,130};
int stepy1[4]={92,72,0,-260};
int stepy2[4]={108,128,200,460};
int st;
for(st=0;st<3;st++)
{
 i = j = k = f = 0;
 while(1)
 {
   c = moon[k++];
   if(c>0x8000)
   {
     o = (c&0x7000)>>12;
     for(l=0;l<(c&0x0FFF);l++)
     {
        if(o<7) buf[i+100*j]=o;
        if(++i>=100){if(++j>=100)j=0;i=0;}
     }
   }
   else
   {
     for(l=0;l<5;l++)
     {
        o = (c&0x7000)>>12;
        if(o<7) buf[i+100*j]=o;
        if(++i>=100){if(++j>=100)j=0;i=0;}
        c <<= 3;
     }
   }
   if(i==0 && j==0)
   {
       int ly=0;
       for(y=stepy1[st];y<stepy2[st];y+=2)
       {
          ly += steps[st];
          if((ly>>8)>=100) break;
          if(y<0 || y>=200) continue;
          p = xodirectline(y);
          pp = xonextline(p);
          int lx = 0;
          for(x=stepx1[st];x<stepx2[st];x++)
          { 
              lx += steps[st];
              if(x>=0 && x<80)
              switch(buf[(lx>>8)+(ly>>8)*100])
              {
                  case 0:
                      p[x] = pp[x] = 0x00;
                      break;
                  case 1:
                      p[x] = 0x00;
                      pp[x] = 0x40;
                      break;
                  case 2:
                      p[x] = pp[x] = 0x40;
                      break;
                  case 3:
                      p[x] = 0x40;
                      pp[x] = 0xE0;
                      break;
                  case 4:
                      p[x] = pp[x] = 0xE0;
                      break;
                  case 5:
                      p[x] = 0xE0;
                      pp[x] = 0xF0;
                      break;
                  case 6:
                      p[x] = pp[x] = 0xF0;
                      break;
              }
              lx += steps[st];
              if(x>=0 && x<80)
              switch(buf[(lx>>8)+(ly>>8)*100])
              {
                  case 1:
                      p[x]|=0x04;
                      break;
                  case 2:
                      p[x] |= 0x04;
                      pp[x] |= 0x04;
                      break;
                  case 3:
                      p[x] |= 0x0E;
                      pp[x] |= 0x04;
                      break;
                  case 4:
                      p[x] |= 0x0E;
                      pp[x] |= 0x0E;
                      break;
                  case 5:
                      p[x] |= 0x0F;
                      pp[x] |= 0x0E;
                      break;
                  case 6:
                      p[x] |= 0x0F;
                      pp[x] |= 0x0F;
                      break;
              }
          }
       }
       xowaitretrace();
       xowaitretrace();
       xowaitretrace();
       if(++f>=127) break;
   }
 }
}
        
/* WIDESCREEN RANDOM SQUARES */

for(y=0;y<200;y++)
    memset(xodirectline(y),0,80);
t1 = xoseconds();
t2 = t1 + 32;
while(t1 <= t2)
{
     int x1 = (rand()%90)*7+5;
     int y1 = (rand()%50)*4+1;
#if 0
     int c = rand()%3;
#else
     int c = ((rand()&63)>(t2-t1))?0:1;
#endif
     if(c==2) xorect(x1,y1,6,3,1);
     else
     {
      xoline(x1,y1,x1+5,y1,c);
      xoline(x1,y1+1,x1+5,y1+1,c);
      xoline(x1,y1+2,x1+5,y1+2,c);
     }
     t1 = xoseconds();
}

/*
k = 0;
w = Xorya_sch_width>>3;
for(j=0;j<Xorya_sch_height;j++){
   p = xodirectline(j);
for(i=0;i<w;i++){
    c = 0;
    m = Xorya_sch_bits[k++];
    for(o=0;o<8;o++) c=(c<<1)|((m&(1<<o))?1:0);
    p[i+15] = c; 
}}
*/

for(y=0;y<200;y++)
    memset(xodirectline(y),0,80);

goto AGAIN;

return 0;
}
