/* touch.c - A.A.Shabarshin (March 2017) */

/* THIS PUBLIC DOMAIN SOURCE CODE IS PROVIDED AS IS */

#include <string.h>
#include <stdio.h>
#include "xorlib.h"
#include "nedofont.h"
#include "resources/scifi1d.c"
#include <peripheral/ports.h>

#define MAXM 50

int main()
{
register int i,j,k,l,o,x,y,c;
register unsigned char *p,*pp;
int tch[4],cc[6];
//int xm[MAXM],ym[MAXM],fm,nm,im,jm,km,rm,mm;
//unsigned int tm;
int w,m,state = 0;
unsigned int t, f = 0;
unsigned long t0,t1,t2;
const char *pshaos;
const char *tshaos;
char str[32];

xoinit(XOMODE_160x200_COL15);

f = 0;

mPORTBSetPinsDigitalOut(BIT_8|BIT_9|BIT_10|BIT_11);
ConfigCNBPullups(CNB8_PULLUP_ENABLE|CNB9_PULLUP_ENABLE|CNB10_PULLUP_ENABLE|CNB11_PULLUP_ENABLE);

while(1)
{
 pshaos = scifi1d;
 if((f++)&1) pshaos++;
 for(y=0;y<200;y++)
 {
   p = xodirectline(y);
//   if(y<100)
   {
      for(i=0;i<80;i++)
      {
/*
".	c #FFFFFF", // 0xF
"+	c #7F7F7F", // 0x5 & 0xA
"@	c #000000", // 0x0
"#	c #8C2D00", // 0x4
"$	c #72D1FF", // 0xB
"%	c #FFAD55", // 0xE
"&	c #90A600", // 0xC
"*	c #0051A9", // 0x1
"=	c #00CA61", // 0x9
"-	c #047800", // 0x8
";	c #FF349D", // 0x6
">	c #7B06C7", // 0x2
",	c #FA86FF", // 0x7
"'	c #6E58FF", // 0x3
")	c #83F837", // 0xD
*/
       switch(*pshaos)
       {
           case '.': c=0xF0; break;
           case '+': c=0x50; break;
           case '@': c=0x00; break;
           case '#': c=0x40; break;
           case '$': c=0xB0; break;
           case '%': c=0xE0; break;
           case '&': c=0xC0; break;
           case '*': c=0x10; break;
           case '=': c=0x90; break;
           case '-': c=0x80; break;
           case ';': c=0x60; break;
           case '>': c=0x20; break;
           case ',': c=0x70; break;
           case '\'': c=0x30; break;
           case ')': c=0xD0; break;
       }
       pshaos++;
       pshaos++;
       switch(*pshaos)
       {
           case '.': c|=0x0F; break;
           case '+': c|=0x05; break;
           case '@': break;
           case '#': c|=0x04; break;
           case '$': c|=0x0B; break;
           case '%': c|=0x0E; break;
           case '&': c|=0x0C; break;
           case '*': c|=0x01; break;
           case '=': c|=0x09; break;
           case '-': c|=0x08; break;
           case ';': c|=0x06; break;
           case '>': c|=0x02; break;
           case ',': c|=0x07; break;
           case '\'': c|=0x03; break;
           case ')': c|=0x0D; break;
       }
       pshaos++;
       pshaos++;
       p[i] = c;
      }
   }
 }
 tch[0]=tch[1]=tch[2]=tch[3]=6;
 mPORTBSetPinsDigitalOut(BIT_8|BIT_9|BIT_10|BIT_11);
 mPORTBClearBits(BIT_8|BIT_9|BIT_10|BIT_11);
 mPORTBSetPinsDigitalIn(BIT_8|BIT_9|BIT_10|BIT_11);
 cc[0] = mPORTBRead();
 cc[1] = mPORTBRead();
 cc[2] = mPORTBRead();
 cc[3] = mPORTBRead();
 cc[4] = mPORTBRead();
 cc[5] = mPORTBRead();
 for(i=0;i<6;i++)
 {
  c = cc[i];
  if((c&BIT_8) && tch[0]==6) tch[0]=i;
  if((c&BIT_9) && tch[1]==6) tch[1]=i;
  if((c&BIT_10) && tch[2]==6) tch[2]=i;
  if((c&BIT_11) && tch[3]==6) tch[3]=i;
 }
#if 1
       for(y=108;y<200;y+=2)
       {
          p = xodirectline(y);
          pp = xonextline(p);
          for(x=0;x<22;x++)
          {
              if(x>=1 && x<10)
              {
                 if(y>=110 && y<148) c=tch[3];
                 else if(y>=152 && y<190) c=tch[2];
              }
              else if(x>=11 && x<20)
              {
                 if(y>=110 && y<148) c=tch[0];
                 else if(y>=152 && y<190) c=tch[1];
              }
              else c=0;
              switch(c)
              {
                  case 0:
                      p[x] = pp[x] = 0x00;
                      break;
                  case 1:
                      p[x] = 0x00;
                      pp[x] = 0x40;
                      break;
                  case 2:
                      p[x] = pp[x] = 0x40;
                      break;
                  case 3:
                      p[x] = 0x40;
                      pp[x] = 0xE0;
                      break;
                  case 4:
                      p[x] = pp[x] = 0xE0;
                      break;
                  case 5:
                      p[x] = 0xE0;
                      pp[x] = 0xF0;
                      break;
                  case 6:
                      p[x] = pp[x] = 0xF0;
                      break;
              }
              switch(c)
              {
                  case 1:
                      p[x]|=0x04;
                      break;
                  case 2:
                      p[x] |= 0x04;
                      pp[x] |= 0x04;
                      break;
                  case 3:
                      p[x] |= 0x0E;
                      pp[x] |= 0x04;
                      break;
                  case 4:
                      p[x] |= 0x0E;
                      pp[x] |= 0x0E;
                      break;
                  case 5:
                      p[x] |= 0x0F;
                      pp[x] |= 0x0E;
                      break;
                  case 6:
                      p[x] |= 0x0F;
                      pp[x] |= 0x0F;
                      break;
              }
          }
       }
#endif
#if 1
 sprintf(str,"D=%i L=%i (%i,%i,%i,%i) ",f-xoframes(),xocurline(),tch[0],tch[1],tch[2],tch[3]);
 xostring(1,24,str);
#endif
 xowaitretrace();
}

/* WIDESCREEN RANDOM SQUARES */

for(y=0;y<200;y++)
    memset(xodirectline(y),0,80);
t1 = xoseconds();
t2 = t1 + 32;
while(t1 <= t2)
{
     int x1 = (rand()%90)*7+5;
     int y1 = (rand()%50)*4+1;
#if 0
     int c = rand()%3;
#else
     int c = ((rand()&63)>(t2-t1))?0:1;
#endif
     if(c==2) xorect(x1,y1,6,3,1);
     else
     {
      xoline(x1,y1,x1+5,y1,c);
      xoline(x1,y1+1,x1+5,y1+1,c);
      xoline(x1,y1+2,x1+5,y1+2,c);
     }
     t1 = xoseconds();
}

while(1);
return 0;
}
