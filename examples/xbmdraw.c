/* xbmdraw.c - A.A.Shabarshin (October 2022) */

/* THIS PUBLIC DOMAIN SOURCE CODE IS PROVIDED AS IS */

#include "xorlib.h"

#include "resources/xorya-mono-640x200-sch.xbm"
#define XBMNAME xorya_mono_640x200_sch_bits
//#define GIGA
//#define COLOR

/*
use macro GIGA for 640x400 XBM:
#define xorya_mono_640x400_width 640
#define xorya_mono_640x400_height 400
static unsigned char xorya_mono_640x400_bits[] = ...
  
comment macro GIGA for 640x200 XBM:
#define xorya_mono_640x200_width 640
#define xorya_mono_640x200_height 200
static unsigned char xorya_mono_640x200_bits[] = ...
 
use macro COLOR to enable colorization of the screen
*/

int main()
{
int i,j,k,y,b,f=0;
unsigned char *p,conv[256];

for(i=0;i<256;i++)
{
    b = 255 - i;
    k = 0;
    for(j=0;j<8;j++)
    {
        k |= (b&1)<<(7-j);
        b >>= 1;
    }
    conv[i] = k;
}

#ifdef COLOR
 xoinit(XOMODE_160x200_COL15); /* gray colors 5 and 10 are identical */

/*
 0 - Default composite mode colors
 1 - Similar to CGA composite mode colors
 2 - Similar to Tandy composite mode colors
 3 - Similar to PCjr composite mode colors
*/
 xopalette(0);
#else
 xoinit(XOMODE_640x200_MONO);
#endif

while(1)
{
 for(y=0;y<200;y++)
 {
   p = xodirectline(y);
   for(i=0;i<80;i++)
   {
#ifdef GIGA
       b = XBMNAME[y*160+f*80+i];
#else
       b = XBMNAME[y*80+i];
#endif
       p[i] = conv[b];
   }
 }
 xowaitretrace();
 if(f) f=0;
 else  f=1;
}
return 0;
}
